DEFAULT: usage

usage:
	@echo "make run"
	@echo "make blog"
	@echo "make containerize"

run:
	hugo server -t terminal

.PHONY: run

blog:
	$(eval NAME=$(shell read -p "Please enter blog title (no spaces) " n; echo $$n))
	$(eval FILE=posts/$(NAME).md)
	hugo new $(FILE)
	@vim content/$(FILE)

.PHONY: blog

containerize:
	podman build -t hugo-site .
	podman push hugo-site quay.io/reavessm/hugo-site
