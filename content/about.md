+++
title = "About"
authorTwitter = "rapscallionreaves" #do not include @
+++

Hey!  My name is Stephen Reaves.  Hopefully this page lets you get to know me
better.

> **Aliases**

reavessm, rapscallionreaves, rpscln

> **Description**

Software Engineer, Technical Consultant, Homelabber, and general nerd

> **Where to find me**

| Link                                                            | Username          | Purpose                                                                                   |
| :----:                                                          | :--               | :--                                                                                       |
| [Github](https://github.com/reavessm)                           | reavessm          | Source code repository for various open source project contributions                      |
| [Gitlab](https://gitlab.com/reavessm)                           | reavessm          | Source code repository for open source projects, eventually mirrors to Github             |
| [Self-Hosted Gitlab](https://gitlab.reaves.dev/reavessm)        | reavessm          | Self-hosted source code repository for open source projects, eventually mirrors to Gitlab |
| [LinkedIn](https://linkedin.com/in/stephen-reaves-40813aa2)     | Stephen Reaves    | Where I can be found for recruiters                                                       |
| [Level1Techs](https://forum.level1techs.com/u/reavessm/summary) | reavessm          | General tech forum where I give and receive advice                                        |
| [Twitch](https://twitch.tv/rapscallionreaves)                   | rapscallionreaves | Mostly game streams, sometimes dev streams                                                |

> **Contact Methods**

[Mastodon](https://fosstodon.org/@rapscallionreaves)

[Email](mailto:reaves735@gmail.com)

> **Projects**

[REA](https://gitlab.com/reavessm/rea) - Reverse Engineering API code generator

[OBI](https://gitlab.com/reavessm/obi) - OSB Installer. Installs arbitrary docker containers to local hard drives

[OSB](https://gitlab.com/reavessm/osb) - Operating System Builder using Gentoo's Catalyst

[MDS](https://github.com/reavessm/mds) - Homelab inspired container init system

> **Cirruculum Vitae**

[View Resume](/resume/StephenReaves_Resume.pdf)

[View CV](/resume/StephenReaves_CV.pdf)

[View Birkman Analysis](/resume/Birkman.pdf)

> **Certifications**

[Exam MB-300](https://www.youracclaim.com/badges/c208ad1f-6e45-4d66-b046-9cfd657da014/public_url): Microsoft Dynamics 365 - Core Finance and Operations

[Exam 535](https://www.youracclaim.com/badges/5c85190e-61bb-4c08-ad0e-028b02bfa96d/public_url): Architecting Microsoft Azure Solutions

[Exam 778](https://www.youracclaim.com/badges/ad10e9eb-cafb-436e-8305-7825255f9fa8/public_url): Analyzing and Visualing Data with Power BI

[Exam 894](https://www.youracclaim.com/badges/4c0cf79d-d8e7-4e39-be19-fa55b0b87858/public_url): Development, Extensions and Deployment for D365FO

> **History**

[2022] Started Georgia Tech's Online Master of Science in Computer Science (OMSCS) program

[2021] Started working at Red Hat as a Software Engineer

[2018] Started working at Blue Horseshoe Solutions, Inc. as a Technical Consultant

[2018] Graduated from the University of South Carolina with major in Computer Science and a minor in business

[2017] Started working in the Housing IT department at the University of South Carolina

[2016] Changed majors to Computer Science, with a minor in Business

[2014] Started College at the University of South Carolina, double majoring in HR & GSCOM with a minor in computer science
