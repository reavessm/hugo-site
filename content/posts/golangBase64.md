+++
title = "Golang Base64"
date = "2022-07-22T14:16:46-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["golang", "programming", "snippets"]
keywords = ["", ""]
description = "How to encode/decode using base64 in golang"
showFullContent = false
readingTime = false
hideComments = false
+++

```go
func Decode(s string) ([]byte, error) {
    decoded, err := base64.StdEncoding.DecodeString(s)
    return decoded, err
}
```

```go
func Encode(b []byte) string {
    return base64.StdEncoding.EncodeToString(b)
}
```
