+++
title = "Intro To Coding"
date = "2023-01-15T07:42:10-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "talks"]
keywords = ["", ""]
description = "Recapping my talk at Ichiban 2023"
showFullContent = false
readingTime = false
hideComments = false
+++

# Intro to Coding

I recently gave a talk at [Ichiban 2023](https://ichibancon.com/).  This talk
goes over the basics of how programming works, including variables, functions,
data structures, and interfaces.  We also ran through one [Advent of
Code](https://adventofcode.com/2022/about) problem.

Here is a follow up, including links to resources to provide programming
exercises.

- [My talk](https://talks.reaves.dev/IntroToCoding/cs101.slide#1)
- [Advent of Code](https://adventofcode.com/2022)
- [Leet Code](https://leetcode.com/)
