+++
title = "Extended Entity Relationship Mapping"
date = "2023-06-15T04:46:55-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 5 of CS-6400"
+++

## Summary

- [Mapping](#mapping)
- [Multi-Instance Type](#multi-instance-type)
- [Relations](#relations)

## Mapping

You can map from EER to Relational models

```graphviz
graph G {
    User [shape=rect; style=rounded];
    Email [label=<<u>Email</u>>];
    Password;
    
    User -- Email;
    User -- Password;
    User -- C;
    C -- D;
    C -- E;
    
    
    subgraph cluster_user{
        label="User";
        color=invis;
        note="C gets lost in translation";
        UR [shape=plain, margin=0, label=<
                <table border="0" cellborder="1" cellspacing="0">
                  <tr>
                    <td><u>Email</u></td>
                    <td>Password</td>
                    <td>D</td>
                    <td>E</td>
                  </tr>
                </table>
            >]
    }
}
```

## Multi-Instance Type

Multi-instance types become a seperate table that has a composite key composed
of the original key plus the mulit-instance type.  The original key is also now
a foreign key on the new table.

```graphviz
graph G {
    User [shape=rect; style=rounded];
    Email [label=<<u>Email</u>>];
    Password;
    F [peripheries=2]
    
    User -- Email;
    User -- Password;
    User -- F;
    
    
    subgraph cluster_user{
        label="User";
        color=invis;
        note="C gets lost in translation";
        UR [shape=plain, margin=0, label=<
            <table border="0" cellborder="1" cellspacing="0">
              <tr>
                <td><u>Email</u></td>
                <td>Password</td>
              </tr>
            </table>
        >];

    }
    
    subgraph cluster_user_f {
        label="User-F";
        color=invis;
        URF [shape=plain, label=<
            <table border="0" cellborder="1" cellspacing="0">
                <tr>
                    <td><u>Email</u></td>
                    <td><u>F</u></td>
                </tr>
            </table>
        >]
    }
}
```

## Relations

```graphviz
graph G {
    rank = same {ET1, ET2, R};
    ET1 [shape=rect; style=rounded];
    A [label=<<u>A</u>>];

    ET1 -- A;
    
    ET2 [shape=rect; style=rounded];
    B [label=<<u>B</u>>];

    ET2 -- B;
    
    R [shape=diamond];
    
    ET1 -- R;
    R -- ET2 [color="black:black"];

    
    subgraph cluster_ET1{
        label="ET1";
        color=invis;
        UR [shape=plain, margin=0, label=<
            <table border="0" cellborder="1" cellspacing="0">
              <tr>
                <td><u>A</u></td>
              </tr>
            </table>
        >];

    }
    
    subgraph cluster_ET2 {
        label="ET2";
        color=invis;
        URF [shape=plain, label=<
            <table border="0" cellborder="1" cellspacing="0">
                <tr>
                    <td><u>B</u></td>
                    <td>A</td>
                </tr>
            </table>
        >]
    }
}
```

Many to Many relations require a join table.

```graphviz
graph G {
    rank = same {ET1, ET2, R};
    ET1 [shape=rect; style=rounded];
    A [label=<<u>A</u>>];

    ET1 -- A;
    
    ET2 [shape=rect; style=rounded];
    B [label=<<u>B</u>>];

    ET2 -- B;
    
    R [shape=diamond];
    
    ET1 -- R [label="M"];
    R -- ET2 [label="N"];

    
    subgraph cluster_ET1{
        label="ET1";
        color=invis;
        e1 [shape=plain, margin=0, label=<
            <table border="0" cellborder="1" cellspacing="0">
              <tr>
                <td><u>A</u></td>
              </tr>
            </table>
        >];

    }
    
    subgraph cluster_et2 {
        label="ET2";
        color=invis;
        e3 [shape=plain,  margin=0,label=<
            <table border="0" cellborder="1" cellspacing="0">
                <tr>
                    <td><u>B</u></td>
                </tr>
            </table>
        >]
    }    
    
    subgraph cluster_r {
        label="R";
        color=invis;
        r [shape=plain,  margin=0,label=<
            <table border="0" cellborder="1" cellspacing="0">
                <tr>
                    <td><u>A</u></td>
                    <td><u>B</u></td>
                </tr>
            </table>
        >]
    }
}
```
