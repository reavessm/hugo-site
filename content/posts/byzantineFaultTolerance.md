+++
title = "Byzantine Fault Tolerance"
date = "2023-03-10T17:04:52-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "omscs", "cs7210"]
description = "Notes about Lesson 16 of CS-7210"
showFullContent = false
readingTime = false
hideComments = false
+++

## Required Readings

- [Practical Byzantine Fault Tolerance](http://www.pmg.csail.mit.edu/papers/osdi99.pdf)

### Optional

- [Blockchain in the Lens of BFT](https://www.usenix.org/conference/atc18/presentation/malkhi)
- [Byzantine General's Problem](https://www.microsoft.com/en-us/research/uploads/prod/2016/12/The-Byzantine-Generals-Problem.pdf)
- [Mapping the Blockchain Project Ecosystem](https://techcrunch.com/2017/10/16/mapping-the-blockchain-project-ecosystem/)
- [The Saddest Moment](https://scholar.harvard.edu/files/mickens/files/thesaddestmoment.pdf)

## Summary

- [Byzantine Failure and Byzantine Generals](#byzantine-failure-and-byzantine-generals)
- [Byzantine Fault Tolerance](#byzantine-fault-tolerance)
- [Practical Byzantine Fault Tolerance](#practical-byzantine-fault-tolerance)
- [pBFT Algorithm](#pbft-algorithm)
- [Byzantine Consensus vs Blockchain](#byzantine-consensus-vs-blockchain)

## Byzantine Failure and Byzantine Generals

Byzantine failures are where a node fails and continues to behave incorrectly

Could be malicious or arbitrary

## Byzantine Fault Tolerance

Goal:
  - Reach consensus
    - Safety, liveness, validity
  - Tolerate up to `f` failures
  - Asynchronous network

Use cryptographic messages to verify message endpoints and to verify the
message has not been tampered with.

To tolerate `f` failures, we need `3f + 1` nodes

To guard against corrupt leader, we need additional checks among participants

To handle [FLP Theorem](/posts/consensusindistributedsystems/#flp-theorem), we
have `bounded delay` (or eventual synchrony) for liveness

## Practical Byzantine Fault Tolerance

`pBFT` algorithm was introduced by Miguel Castro and Barbara Liskov in 1999

High performance algorithm

Setup:
  - System is a replicated service
    - `n = 3f + 1`
    - Primary/Leader and backups
  - All operations are within a `view`
  - Each replica maintains the service state
    - Must be consistent
    - State includes:
      - Service State
      - Message Log
      - Current View
  - Communication integrity guaranteed cryptographically
    - Digests
    - Public keys

Quorum = `n - f` nodes

## pBFT Algorithm

Client request -> pre-prepare -> prepare -> commit -> reply

### PrePrepare

Primary node multicasts `pre-prepare` piggybacked with message to backups

The message is stored in the log

`pre-prepare` request includes
  - sequence number
  - digest of message
  - signature

Backups accept pre-prepare request if:
  - Signature and digest are correct
  - View is correct
  - Sequence number is new
  - Sequence number is between two watermarks

Then replica enters prepare phase

### Prepare

Multicasts `prepare message` and adds the message to log, then waits for `2f`
matching prepares which have same view, sequence number, and digest.  Then it
moves to commit stage.

### Commit

Replica multicasts a `commit message` and adds the message to log, waits for
`2f +1` commit messages, then sends reply to client.

## Byzantine Consensus vs Blockchain

Blockchain's distributed ledger is similar to Paxos's replicated log

Can we build Blockchain network with just pBFT?

| Pros                    | Cons                             |
|:-----------------------:|:--------------------------------:|
| Decentralized consensus | Tolerate `f` faults in `N=3f+1`? |
| Byzantine failures      | Communication costs `O(n^3)`     |
| Unreliable network      |                                  |

Not everyone is able to participate in maintaining the ledger
  - Only the miners that have Proof-of-Work

There are also incentives for good behavior
