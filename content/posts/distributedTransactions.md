+++
title = "Distributed Transactions"
date = "2023-02-20T06:14:25-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "omscs", "cs7210"]
description = "Notes about Lesson 9 of CS-7210"
showFullContent = false
readingTime = false
hideComments = false
+++

## Required Readings

- [Spanner](https://www.usenix.org/system/files/conference/osdi12/osdi12-final-16.pdf)

### Optional

- [Amazon Aurora](https://pages.cs.wisc.edu/~yxy/cs839-s20/papers/aurora-sigmod-18.pdf)
- [Amazon Aurora Design Considerations](https://www.amazon.science/publications/amazon-aurora-design-considerations-for-high-throughput-cloud-native-relational-databases)
- [Big Table](https://research.google/pubs/pub27898/)
- [Megastore](https://research.google/pubs/pub36971/)
- [TrueTime and External Consistency](https://cloud.google.com/spanner/docs/true-time-external-consistency)

## Summary

- [What are Distributed Transactions](#what-are-distributed-transactions)
- [Spanner Brief](#spanner-brief)
- [Spanner Stack](#spanner-stack)
- [Consistency Requirements](#consistency-requirements)
- [True Time](#true-time)
- [Ordering Write Transactions with TrueTime Timestamps](#ordering-write-transactions-with-truetime-timestamps)
- [Read Transactions](#read-transactions)
  - [Read Now](#read-now)
  - [Read Later](#read-later)
- [No TrueTime](#no-truetime)
- [Another Distributed DB Example: AWS Aurora](#another-distributed-db-example-aws-aurora)


## What are Distributed Transactions

Group of operations that are applied together, over multiple nodes

ACID properties

Useful for concurrency and fault tolerance

Outcomes are `commit` or `abort`

Typically done with coordinator (leader)

## Spanner Brief

Data is geographically distributed

Data is sharded within a locations

Data is replicated across multiple sites within a location

## Spanner Stack

3 [Colossus
FS](https://cloud.google.com/blog/products/storage-data-transfer/a-peek-behind-colossus-googles-file-system)
nodes, which are optimized for reads and appends

[Big Table](https://cloud.google.com/bigtable/docs/overview) on top of
Colossus which exports files into application specific data model

Data model is grouped into `tablets`, which are groups of related objects

Megastore, which is a Replicated State Machine (w/ Paxos) per tablet

- 2 phase locking concurrency control
  - 2PC for cross-replica set transactions to determine lock

## Consistency Requirements

Read operations on are replica of state, not current state, to prevent blocking

> External consistency := order of events in the system must match the
> order in which events appeared in the real world clock

Requires strict serializability and common global clock

## True Time

TT != absolute real time

TT is uncertainty around real time

You can define:

- `TT.after(t)`, true if time _t_ has definitely passed
- `TT.before(t)`, true if time _t_ has definitely not arrived
- `TT.now()` which returns `TTInterval[earliest, latest]`
  - Narrowest scope of what time could be

Periodic probing of master clock servers in datacenter

- GPS and Atomic clocks + 2*ε

1. Acquire locks
1. Pick time _s_ = `TT.now().latest`
1. Do operation
1. Wait until `TT.now().earliest > s`
1. Release locks

Commit wait may be much longer than actual operation

## Ordering Write Transactions with TrueTime Timestamps

> Pessimistic Locking := acquiring all locks upfront to prevent later conflicts

On a single replica set:

1. Acquire Locks
1. Pick TT Timestamp _s_
1. Leader starts consensus
1. Achieve consensus
1. Commit wait done
1. Release locks
1. Notify slaves

On transactions across multiple replica sets, we add 2PC:

1. Each replica acquires locks
1. Each replica picks TT Timestamp _s_
1. Transaction starts executing as before
1. Each replica set starts logging updates they are to perform
1. Done logging, each node sends it's _s_ to transaction coordinator to
   compute overall timestamp _s_ for transaction
1. Commit wait
1. Release locks on transaction coordinator
1. Notify participants
1. Participants release locks

## Read Transactions

2 Types of transactions, `Read Now` and `Read Later`

### Read Now

Still need to be externally ordered

Use leader info to determine a "safe" timestamp

- For single replica set, just use Paxos leader
- For multi-replica, consider all at TxMgr

Potentially delay until it's safe time is reached

Prepared but not committed transactions delay reads

### Read Later

Simply read at specific timestamp

Timestamp saves us from using distributed cut algorithm

## No TrueTime

GPS + atomic clocks => few ms uncertainty

- ~5,7 ms
- Just wait it out

What if we don't have GPS + atomic clocks?

[NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol) => ~100ms uncertainty

Make it visible when/if external consistency is needed, only perform
expensive wait when it's needed

[Cockroach
DB](https://www.cockroachlabs.com/docs/stable/architecture/transaction-layer.html#max-clock-offset-enforcement)
does this

Snapshot isolation (or Multi-Version Concurrency Control) allows
transactions to be interleaved, even when operating on same data

To guarantee correctness:

- Transactions read from a snapshot of distributed state
- Sequence of snapshots are serializable (i.e. No cycles)

## Another Distributed DB Example: AWS Aurora

Follows Primary-Replica architecture

Primary handles reads and writes, multiple replicas handle reads only

Designed for availability

Replicated across 3 zones, 2 nodes per zone

6 replicas = I/O Amplification (each write turns into 6 writes)

Log replication over S3 solves this
