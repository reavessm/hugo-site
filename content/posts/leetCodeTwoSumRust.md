+++
title = "Leet Code - Two Sum - Rust"
date = "2022-08-05T16:21:10-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["rust", "programming", "leet-code", "snippets"]
keywords = ["", ""]
description = "Leet Code answer to 'Two Sum' problem, written in Rust"
showFullContent = false
readingTime = false
hideComments = false
+++

```rust
struct TestCase {
    nums: Vec<i32>,
    target: i32,
    answer: Vec<i32>,
}

fn main() {
    let tcs = Vec::from([
                        TestCase {
                            nums: Vec::from([2, 7, 11, 15]),
                            target: 9,
                            answer: Vec::from([0,1]),
                        },
                        TestCase {
                            nums: Vec::from([3, 2, 4]),
                            target: 6,
                            answer: Vec::from([1,2]),
                        },
                        TestCase {
                            nums: Vec::from([3,3]),
                            target: 6,
                            answer: Vec::from([0,1]),
                        }
    ]);

    for i in tcs.iter() {
        println!("{:?} {:} {:?} -> {:?} == {:?}", i.nums, "/", i.target, two_sum(i.nums.to_vec(), i.target), i.answer);
    }
}

pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    for i in 0..nums.len()-1 {
        for j in i+1..nums.len() {
            if nums[i] + nums[j] == target {
                return Vec::from([i as i32, j as i32])
            }
        }
    }
    return Vec::from([])
}
```
