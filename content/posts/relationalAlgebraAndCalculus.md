+++
title = "Relational Algebra And Calculus"
date = "2023-06-16T19:26:16-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 6 of CS-6400"
+++

## Summary

<++>

## Closed Algebra

> Closed Algebra := A set of operations where the input operands and output
> operands are of the same type.

## Operators

https://cs.brown.edu/courses/csci1270/website_2020/static/files/cs127_cheatsheet.pdf

$katex R \cup S katex$ is a union.

$katex R \cap S katex$ is an intersection.

$katex R \setminus S katex$ is the set difference.

$katex R \times S katex$ is the Cartesian Product.

$katex \pi A_1, A_2, \dots, A_n (R) katex$ is the projection (eliminating columns).

$katex \sigma expression (R) katex$ is the selection (eliminating rows).

$katex R * S katex$ or $katex R \bowtie S katex$ is the natural join.

$katex R \div S katex$ is divide by (universal quantification).

$katex \rho [A_1 B_1, \dots, A_N B_N] katex$ is the rename operator.

## Selection Example

$katex \sigma expression (R) => katex$ select all records from set
$katex R katex$ that match $katex expression katex$.

$katex \sigma CurrentCity = HomeTown OR HomeTown = 'Atlanta' (RegularUser) katex$

## Relational Calculus

> {t | P(t)} := Set of tuples t that satisfy predicate P(t)

Predicates are built from atoms

> Range Expression: $katex t \in R katex$ or $katex R(t) katex$ denote that t is a tuple of relation R.

> Attribute Value: t.A denotes the value of t on attribute A.

> Constants are defined by c

> Comparison operators: $katex \theta =, \neq \leq, \geq, <, > $katex

> Atoms := $katex t \in R, r.A \theta s.B, r.A \theta c katex$
