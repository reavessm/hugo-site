+++
title = "Grdr"
date = "2023-01-16T09:15:21-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "rust"]
description = "Rust full stack web app"
showFullContent = false
readingTime = false
hideComments = false
draft = true
+++

# Grdr

Hugo for rust

## Installing

### Installing dependencies

#### Fedora

```bash
sudo dnf install rust-std-static-wasm32-unknown-unknown protobuf-compiler
```

#### Gentoo

```bash
echo 'dev-lang/rust clippy' >> /etc/portage/package.use/rust
echo 'dev-lang/rust rls' >> /etc/portage/package.use/rust
echo 'dev-lang/rust rustfmt' >> /etc/portage/package.use/rust
echo 'dev-lang/rust rust-src' >> /etc/portage/package.use/rust
echo 'dev-lang/rust system-bootstrap' >> /etc/portage/package.use/rust
echo 'dev-lang/rust wasm' >> /etc/portage/package.use/rust

sudo emerge --ask dev-lang/rust dev-libs/protobuf
```
