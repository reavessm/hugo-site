+++
title = "Fundamentals Of Databases"
date = "2023-05-15T06:28:44-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 1 of CS-6400"
showFullContent = false
readingTime = false
hideComments = false
+++

## Summary

- [Why use Models](#why-use-models)
- [Message to Model Makers](#message-to-model-makers)
- [To Use or Not To Use](#to-use-or-not-to-use)
- [Data Modeling](#data-modeling)
- [Process Modeling](#process-modeling)
- [Data Models](#data-models)
- [Example of Data Models](#example-of-data-models)
- [Relational Model Data Structures](#relational-model-data-structures)
- [Relational Model Constraints ](#relational-model-constraints-)
- [Data Model Operations](#data-model-operations)
- [Keys and Identifiers](#keys-and-identifiers)
- [Integrity and Consistency](#integrity-and-consistency)
- [Null Values](#null-values)
- [Surrogates - Things and Names](#surrogates---things-and-names)
- [ANSI/SPARC](#ansi/sparc)
  - [Conceptual Schemas](#conceptual-schemas)
  - [External Schemas](#external-schemas)
  - [Internal Schemas](#internal-schemas)
- [Physical Data Independence](#physical-data-independence)
- [Logical Data Independence](#logical-data-independence)
- [ANSI/SPARC DBMS Framework](#ansi/sparc-dbms-framework)
- [Metadata](#metadata)

## Why use Models

Models can be useful when we want to examine or manage part of the real world.

The costs of using a model are often considerably lower than the cost of using
or experimenting with the real world itself.

Models also use conventions to increase speed in making a point.

## Message to Model Makers

Users of a model must have a certain amount of knowledge in common
(conventions).

A model is:
  - a means of communication
  - only emphasizes selected aspects
  - is described in some language
  - can be erroneous
  - may have features that do not exist in reality
    - contour lines on a mountain

## To Use or Not To Use

Database Management Systems (DBMS) are good at:
  - maintaining data intensive applications (as opposed to process intensive)
  - persistent storage of data
  - centralized control of data
  - control redundancy
  - control consistency and integrity
    - can you contrive contradictions from within the database itself
  - multi-user support
  - sharing of data
  - data documentation (schema)
  - data independence
  - control of access and security
  - backup and recovery utility

Why not use a DBMS:
  - initial investment in hardware, software, and training can be high
  - generality is not always needed
    - overhead for security, concurrency control, and recovery can be too high
  - data and application are simple and stable
  - real-time requirements cannot be met by it
  - maybe multi-user access is not needed

## Data Modeling

The `model` represents a perception of structures of reality.

The `data modeling process` is to fix a perception of structures of reality and
represent this perception.

In the data modeling process, we select aspects and we abstract.

## Process Modeling

The use of the model reflects processes of reality.

Processes may be represented:
  - embedded in program code
  - executed ad hoc

## Data Models

Data models contain:
  - data structures
  - constraints
  - operations
  - keys and identifiers
  - integrity and consistency
  - null values
  - surrogates

DB architecture and DBMS architecture are concerned with the layers of the
system.

## Example of Data Models

> A data model is not the same as a model of data

A database is a model of the structures of reality.

A data model is the tool (or formalism) used to create the model.

A data model includes:
  - data structures
  - integrity constraints
  - operations

## Relational Model Data Structures

Data is represented in tables.  Tables consist of a name, and a matrix where
columns a fields (consisting of name and type) and rows are entries.

Example:

| Email (varchar(50)) | BirthDate (datetime) | Hometown (varchar(50)) | Salary (integer) |
| :---                | :---:                | :---:                  | ---:             |
| user1@gt.edu        | 1985-11-07           | Atlanta                | 10,000           |
| user2@gt.edu        | 1969-11-28           | Austion                | 10,000           |
| user3@gt.edu        | 1967-11-03           | Portland               | 12,000           |
| user4@gt.edu        | 1988-11-15           | Atlanta                | 13,000           |
| user5@gt.edu        | 1973-03-12           | Portland               | 14,000           |
| user6@gt.edu        | 1988-11-09           | Atlanta                | 15,000           |

> Degree := Number of columns

> Schema := The aspect of the table that is considered stable, or not expected
> to change over time

> State := The aspect of the table that is expected to change over time

## Relational Model Constraints 

Constraints express rules that cannot be expressed by the data structures alone

Examples:
  - Emails must be unique
  - Must not be null
  - BirthDate must be after 1900-01-01
  - Hometown must by city in US

## Data Model Operations

Operations support change and retrieval of data.

## Keys and Identifiers

Keys are uniqueness constraints

## Integrity and Consistency

Integrity answers the question "Does the db reflect reality well?"

Consistency answers the question "Is the db without internal
conflicts/contradictions?"

## Null Values

| Email (varchar(50)) | BirthDate (datetime) | Hometown (varchar(50)) | SSRegist     | Sex   | Salary (integer) |
| :---                | :---:                | :---:                  | :---:        | :---: | ---:             | 
| user1@gt.edu        | 1985-11-07           | Atlanta                | Yes          | M     | 10,000           |
| user2@gt.edu        | 1969-11-28           | Austion                | INAPPLICABLE | F     | 10,000           |
| user3@gt.edu        | 1967-11-03           | Portland               | INAPPLICABLE | F     | 12,000           |
| user4@gt.edu        | 1988-11-15           | Atlanta                | INAPPLICABLE | F     | 13,000           |
| user5@gt.edu        | 1973-03-12           | Portland               | INAPPLICABLE | F     | 14,000           |
| user6@gt.edu        | 1988-11-09           | Atlanta                | No           | M     | 15,000           |

Null values can be used for "unknown", but not for "Inapplicable"

## Surrogates - Things and Names

In name-based representation you are what is known about you, no more, no less.

Surrogate keys solve this problem by adding an artificial ID.  This allows you
to change everything else about a record and still determine if it is the same
record.

## ANSI/SPARC

We already separate data and schema, why not separate schema into conceptual
and internal schemas?

### Conceptual Schemas

Conceptual Schema describes all conceptually relevant, general, time-invariant,
structural aspects of reality, and excludes aspects of data representation and
physical organization, and access.

This is typically how we normally think of schemas.

### External Schemas

Describes parts of the information in the conceptual schema in a form
convenient to a particular user group's view.  It is derived from the
conceptual schema.

This is typically how we think of views.

### Internal Schemas

Describes how the information described in the conceptual schema is physically
represented to provide the overall best performance.

This is typically how we think of indexes.

## Physical Data Independence

Physical data independence is a measure of how much the internal schema can
change without affecting the application programs.

This is similar to encapsulation in OOP.

## Logical Data Independence

Logical data independence is a measure of how much the conceptual schema can
change without affecting the application programs.

## ANSI/SPARC DBMS Framework

```graphviz
digraph G {
    node [style=rounded, shape=rect];
    edge [arrowhead=none];


    subgraph cluster_query{
        label="Query Transformer";
        labelloc="b";
        labeljust="l";
        subgraph 0 {
            rank=same;
            rankdir=LR;
            bgcolor=gray;
    
            user [shape=hexagon, style=solid];
            si [label="Storage Internal"];
            is [label="Internal Schema"];
            coe [label="Conceptual/External"];
            
            data -> si -> is -> coe;
            coe -> user [label="Queries"];
    
        }
    }
    subgraph cluster_compiler {
        label="Schema Compiler"
        labelloc="t";
        labeljust="l";
        subgraph 1 {
            rank=same;
            rankdir=LR;
            
            metadata [shape=triangle, style=solid];
            isp [label="Internal Schema Processor"];
            esp [label="External Schema Processor"];
            
            isp -> metadata -> esp;
        }
        subgraph 2 {
            rank=same;
            rankdir=LR;
            
            dba [shape=hexagon, style=solid, label="Database Administration"];
            asa [shape=hexagon, style=solid, label="Application System Administrator"];
            csp [label="Conceptual Schema Processor"];
            
            dba -> csp -> asa;
        }
        
        subgraph 3 {
            ea [shape=hexagon, style=solid, label="Enterprise Administrator"];
        }
    }
    
    rank=max;
    rankdir=TB;
    
    ea -> csp;
    dba -> isp [label="Change schema"];
    csp -> metadata;
    asa -> esp;
    metadata -> si;
    metadata -> is;
    metadata -> coe;
}
```

Framework came out as early as 1975.

## Metadata

- Systems Metadata:
  - Where did data come from
  - How data was changed
  - How data are stored
  - How data are mapped
  - Who owns data
  - Who can access data
  - Data usage history
  - Data usage statistics
- Business Metadata:
  - What data are available
  - Where are data located
  - What the data means
  - How to access the data
  - Predefined reports
  - Predefined queries
  - How current the data are

System metadata are critical in a DBMS.

Business metadata are critical in a data warehouse.
