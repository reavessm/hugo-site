+++
title = "Database Application Methodology"
date = "2023-05-29T06:25:08-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 2 of CS-6400"
showFullContent = false
readingTime = false
hideComments = false
+++

## Summary

- [Assumptions](#assumptions)
- [The Software Process](#the-software-process)
- [Overview of Methodology](#overview-of-methodology)
- [Analysis - Information Flow Diagram](#analysis---information-flow-diagram)
- [In and Out](#in-and-out)

## Assumptions

- Business processes are well-designed
- Documents are known
  - Documents are input to, or output from a db application
- Tasks are known
  - Tasks are the processing done using the documents
- System boundary is known
- One db schema unifying all views can be designed
  - Difficult with different interests, goals, power, politics

## The Software Process

Waterfall:
  - Business Process (re-)design
  - **Analysis**
  - **Specification**
  - **Design**
  - **Implementation**
  - Testing
  - Operation
  - Maintenance

## Overview of Methodology

In db application development process, data is always first.

- Analysis
  - Information Flow Diagram
- Specification
  - Tasks
  - ER Diagram
- Design
  - Abstract Code w/ SQL
  - Relational Schema
- Implementation
  - PHP Code w/ SQL
  - MySQL Relational Platform

## Analysis - Information Flow Diagram

Maps documents to tasks to db

Example:

```graphviz
digraph G {
    beautify=true;

    subgraph cluster_key{
        rank=min;
        rankdir=LR;
        style=filled;
        shape=circle;
        color=lightgray;
        label="Key";
        labelloc="t";
        
        document [shape=rect];
        task [shape=oval];
        
        document -> task [label="Info\nFlow"]
    }

    subgraph cluster_documents {
        style="filled";
        bgcolor="yellow";
        node [shape=rect];

        label="Documents";
        
        d1;
        d2;
        d3;
        d4;
        d5;
        d6;
    }
    
    subgraph cluster_system {
        style=filled;
        bgcolor="lightblue";
        node [shape=oval];

        label="System";
        
        t1;
        t2;
        t3;
        t4;
        
        db [label="Database" shape=diamond];
        
        d1 -> t1;
        d2 -> t1 [dir=both];
        t2 -> d3;
        d4 -> t2 [dir=both];
        d5 -> t3 [dir=both];
        d6 -> t4;
        
        t1 -> db [dir=both];
        t2 -> db [dir=both];
        t3 -> db [dir=both];
        t4 -> db;
    }

    labelloc=b;
    labeljust=l;
    label="Notes:\l  - Documents\l    - D1 is the first document\l    - D2 is the second document\l";
}
```

IFD for project:

```diagraph
digraph G {
    beautify=true;
    label="Information Flow Diagram";
    labelloc="t";

    // Key
    subgraph cluster_key {
        bgcolor=lightgray;
        color=white;
        label="Key";
        labelloc="t";

        document [shape=rect];
        task [shape=oval];

        document -> task [label="Info\nFlow"]
    }

    // Tasks and DB
    subgraph cluster_system {
        bgcolor="lightblue";
        node [shape=oval];
        label="System";

        db [label="Database" shape=cylinder];

        login    [label="Login"];
        register [label="Register"];
        ep       [label="Edit Profile"];
        vp       [label="View Profile"];
        vf       [label="View Frields"];
        vs       [label="View Status/Updates/Comments"];
        as       [label="Add status update"];
        ac       [label="Add comment"];
        sf       [label="Search for Friends"];
        rf       [label="Request Friend"];
        vfr      [label="View/Accept/Reject Friend Request"];
    }

    // Documents
    node    [shape=rect];
    lif     [label="Log In Form"];
    rf      [label="Registration Form"];
    pef     [label="Profile Edit Form"];
    profile [label="Profile"];
    fl      [label="Friends List"];
    sc      [label="Status/Comments"];
    fs      [label="Friend Search and Results"];
    fr      [label="Friend Request"];
    pfl     [label="Pending Friends List"];


    // Connections
    lif -> login;
    db -> login;

    rf -> register -> db;

    pef -> ep -> db [dir=both];

    db -> vp -> profile;

    db -> vf -> fl;

    db -> vs -> sc;
    sc -> as -> db;
    sc -> ac -> db;

    db -> sf;
    sf -> fs [dir=both];

    fr -> rf -> db;

    pfl -> vfr -> db [dir=both];
}
```

## In and Out

- Everything in the database must come from somewhere
- Everything on the input documents must go somewhere
- Everything in the database must be used for something
- Everything on the output documents must come from somewhere
