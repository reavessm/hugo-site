+++
title = "Building Custom Images With Openshift"
date = "2023-02-01T13:11:58-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["openshift", "docker", "podman"]
description = "Building custom images using BuildConfig, from a public image, to a private is"
showFullContent = false
readingTime = false
hideComments = false
#draft = true
+++

I was working with some containers recently on my
[Openshift](https://www.redhat.com/en/technologies/cloud-computing/openshift)
cluster and I wanted to
[debug](https://docs.openshift.com/container-platform/4.12/support/troubleshooting/investigating-pod-issues.html#starting-debug-pods-with-root-access_investigating-pod-issues)
a pod.  I tried to run a [fedora
image](https://quay.io/repository/fedora/fedora) by specifying `oc debug
pod/<podName> --image quay.io/fedora/fedora --as-root=true`.  The container
created everything fine, but when I tried to
[ping](https://en.wikipedia.org/wiki/Ping_(networking_utility)) a resource, I
found that the default image didn't have `ping` installed.  Let me walk you
through how I decided to solve this problem.

## ImageStream

I decided to build a custom
[ImageStream](https://docs.openshift.com/container-platform/4.12/openshift_images/index.html#images-imagestream-use_overview-of-images)
(is) that was essentially Fedora + the packages I needed.  An `is` is basically
an abstraction over the standard
[container](https://www.redhat.com/en/topics/containers) that allows for rapid
consumption in an `OpenShift` cluster.  I could have [built my container with
Gitlab CICD](/posts/autobuildcontainersgitops/), but I really only need this to
be internal to the cluster.  This is exactly what `is`s are for. Actually,
we'll need two of them.

## Overview

Basically, the workflow will look like this, the first `is` will simply mirror
`quay.io/fedora/fedora:latest`.  We can set this to automatically check for a
newer upstream version every 15 or so minutes and refresh the local version if
needed.  This also works as a image cache, since we can pull from the `is`
multiple times and not mess with our [docker pull
limit](https://www.docker.com/increase-rate-limits/).

We also have to define an `is` for our resulting image.  For the purpose of
this blog we'll call them `is/fedora-input` and `is/fedora-output`.

In the middle we have a
[BuildConfig](https://docs.openshift.com/container-platform/4.12/cicd/builds/understanding-buildconfigs.html)
(BC) that applies the transformations.  There's actually quite a lot of options
here, but we are simply going to use an inline-dockerfile.

## Result

Here is the config that I was able to use to get the tools I needed for debugging:

```yaml
apiVersion: image.openshift.io/v1
kind: ImageStream
metadata:
  name: fedora-input
spec:
  lookupPolicy:
    local: true
  tags:
  - name: latest
    from:
      kind: DockerImage
      name: quay.io/fedora/fedora
    # importPolicy is what determines if we should check for a newer image
    importPolicy:
      scheduled: true
    referencePolicy:
      type: Source
---
apiVersion: image.openshift.io/v1
kind: ImageStream
metadata:
  name: fedora-output
spec:
  lookupPolicy:
    local: true
---
kind: BuildConfig
apiVersion: build.openshift.io/v1
metadata:
  name: "fedora-builder"
spec:
  runPolicy: "Serial"
  strategy:
    dockerStrategy: {}
    type: Docker
  source:
    type: Dockerfile
    dockerfile: |
      FROM image-registry.openshift-image-registry.svc:5000/<namespace>/fedora-input:latest

      RUN dnf install -y iputils postgresql
  output:
    to:
      kind: "ImageStreamTag"
      name: "fedora-output:latest"
  triggers:
    # This line simply says to run a build if we change the overal yaml
    - type: ConfigChange
    # This piece of magic starts a build everytime the `ImageStream` is changed.
    - type: ImageChange
      imageChange:
        from:
          kind: ImageStreamTag
          name: fedora-input:latest
```

So now when Fedora publishes new image, that triggers a change in my local
`is/fedora-input`, which kicks off a new build, which finally updates my
`is/fedora-output`.  This way, I can use `is/fedora-output` as my debug image
and it's always up to date with exactly the tools I need.
