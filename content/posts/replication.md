+++
title = "Replication"
date = "2023-02-11T07:58:10-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "omscs", "cs7210"]
description = "Notes about Lesson 6 of CS-7210"
showFullContent = false
readingTime = false
hideComments = false
+++

## Required Readings

- [Chain Replication for Supporting High Throughput and Availability](https://www.cs.cornell.edu/home/rvr/papers/OSDI04.pdf)

### Optional

- [Object Storage on CRAQ](https://www.usenix.org/legacy/event/usenix09/tech/full_papers/terrace/terrace.pdf)

## Summary

- [Goal of Replication](#goal-of-replication)
- [Replication Models](#replication-models)
- [Replication Techniques](#replication-techniques)
- [Replication and Consensus](#replication-and-consensus)
- [Chain Replication](#chain-replication)
- [CRAQ](#craq)
- [CRAQ vs CR Scalability](#craq-vs-cr-scalability)

## Goal of Replication

State available on more that one node

Different nodes can provide the same service

### Benefits

- Fault Tolerance
- Availability
  - Disaster Recovery
- Scalability

## Replication Models

### Active

Each replica is active

Each replica can:

- Read requests
- Ensure replication updates

### Standby (Primary/Backup)

Only one replica is active/primary at one time

Updates are kept consistent so failover can be fast

## Replication Techniques

### State Replication

Execute updates on one replica, copy state changes to update other replicas

Good when execution is expensive

State may be large and/or hard to identify where all the updates are

### Replicated State Machine

Copy each (log of) operation to each replica and execute to produce same state
update

Only works on deterministic operations

Sending operation logs can be faster than sending large state

Re-execution might be expensive

## Replication and Consensus

Ensure consensus for updates to replicas

Ordering and visibility of updates:

- Depends on the consistency model
- Enforced at granularity of individual updates or groups of updates
  (transactions)

## Chain Replication

Normally, cost of consensus is proportional to number of nodes

Write -> R1 -> R2 -> R3 -> Ack

Read -> R3 -> Ack

Pros:

- Leader scalability
- High write throughput due to pipelining
- Strong consistency

Cons:

- Not read-optimized
- Low utilization from other nodes during reads

## CRAQ

Problem with Chain Replication: Reads limited to tail replica only

Enter Chain Replication with Apportioned Queries (CRAQ)

> apportioned := divided among the chain

> queries := read operations

writes continue to be handle by head replica

Reads are divided among all the replicas

Each node maintains new and old values, serves old value until new value is
fully replicated down the chain

## CRAQ vs CR Scalability

Testing read throughput

Experiment:

- 3 or 7 replicas
- up to 100 writes/s at head

Observations:

- CRAQ (3 or 7) can scale to higher read throughput, even as write load
  increases
- CRAQ throughput scales with the increase of the number of replicas in the
  chain
- CRAQ-7 is faster than CRAQ-3 is faster than CR
