+++
title = "Golang GZIP"
date = "2022-07-22T09:30:48-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["golang", "programming", "snippets"]
keywords = ["", ""]
description = "How to (de)compress a file in golang with GZIP"
showFullContent = false
readingTime = false
hideComments = false
+++

```go
func Compress(b []byte) ([]byte, error) {
    buf := &bytes.Buffer{}

    // You can choose `gzip.BestSpeed` or `gzip.BestCompression`
    gzipWriter, err := gzip.NewWriterLevel(buf, gzip.BestSpeed)
    if err != nil {
        return nil, err
    }

    _, err := gzipWriter.Write(b)
    if err != nil {
        return nil, err
    }

    if err = gzipWriter.Close(); err != nil {
        return nil, err
    }

    return buf.Bytes(), nil
}
```

```go
func DeCompress(b []byte) ([]byte, error) {
	buf := &bytes.Buffer{}
	gReader, err := gzip.NewReader(buf)
	if err != nil {
		return nil, err
	}

	_, err = gReader.Read(b)
	if err != nil {
		return nil, err
	}

	if err = gReader.Close(); err != nil {
		return nil, err
	}

    return buf.Bytes(), nil
}
```
