+++
title = "OSB"
date = "2022-03-21"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["linux", "golang", "open-source", "programming"]
description = "A 'simple' container-based Gentoo build system"
showFullContent = false
readingTime = false
hideComments = false
+++

# OSB

[OSB](https://gitlab.com/reavessm/osb) is an **O**perating **S**ystem **B**uilder.  The goal of OSB faciliate the creation of custom [stage 4](https://wiki.gentoo.org/wiki/Stage_tarball) system, based on [Gentoo](https://www.gentoo.org/get-started/about/).  You can integrate OSB into your CICD pipelines and have a constantly updating OS ready to go.  Couple OSB with [OBI](/20220322_OBI) and you can have the latest Gentoo goodness ready to install and play with at all times.

## How does it work?

OSB heavily relies on [Catalyst](https://wiki.gentoo.org/wiki/Catalyst) to build the multiple stages.  This way we can build the system completely from scratch.

Lets take a basic look at the files involved:

```
/home/sreaves/Src/OSB/
├── configs <-------------------- Generic config files for catalyst and portage
├── containerfiles <------------- Files used to build container images
│   ├── catalyst.Containerfile
│   ├── installer.Containerfile
│   ├── portage.Containerfile
│   ├── stage1
│   ├── stage2
│   ├── stage3
│   └── stage4
├── Makefile <------------------- Where the magic happens
├── portage <-------------------- Stage specific portage configs
│   ├── livecd-stage1
│   ├── stage1
│   ├── stage2
│   ├── stage3
│   └── stage4
└── specs <---------------------- Stage specific catalyst configs
    ├── common.spec
    ├── flags.spec
    ├── livecd-stage1
    ├── livecd-stage2
    ├── stage1
    ├── stage2
    ├── stage3
    └── stage4
```

### Stages

Everything revolves around the idea of `stages`.  The OS is built in stages to allow us to reuse code where we need to, diverge code where we need to, but also ensure a certain level of code quality and system features.  Each stage has its own specific purpose

#### Stage 0

Stage 0, or the `seed stage`, isn't technically a stage defined by catalyst, rather it is whatever system you are coming from.  If you trying to build a Gentoo system from your Windows laptop, your laptop would be considered the seed stage.  You would download the source code and compile it using whatever compiler your computer comes with.  The big defining factor of this stage is the uncertainability.  You can start the build process from whatever system you want, and whatever OS you want, and whatever build tools you want.  This causes the resulting binary built from the source code to be slightly different, even if you use the same source code.  For the sake of simplicity, we try to use existing Gentoo docker containers as a seed stage when possible.

#### Stage 1

Stage 1 is the basic system tools.  Also called the toolchain.  This includes things like gcc, binutils, libtool, clang, etc.  This is a simple stage.  Code is built from the seed stage and wrapped in a tarball

#### Stage 2

Stage 2 is the same tools from stage 1, but rebuilt with the stage 1 tools instead of the stage 0 tools.

This should be a simple stage, BUT we can't gaurantee the output of the seed stage (because we can't gaurantee the seed system) so we have to rebuild the system tools.  You can think of these stages as a series of inputs and outputs.  Then we would have stage 1 taking in an unknown source and outputting a known binary.  Although we know the features the stage 1 binary can produce, we don't know the features it has been build with.  Then stage 2 would input the known binary from stage 1, rebuilding itself, then outputting a known binary with known features.

For example, let's say the the Windows compiler doesn't allow for [Link-Time Optimization](https://wiki.gentoo.org/wiki/Clang#Link-time_optimizations_with_Clang) (I don't know if it does or doesn't but let's assume it doesn't).  If you built clang, enabling LTO, from your Windows computer, clang would be able to produce code that is optimized with LTO, but clang *itself* would be unoptimized and would thus take longer to compile your code.  So before we get to compiling your code, we build a clang binary that is optimized and can produce optimized code from a clang binary that is unoptimized but can still procude optimized code.

It's also nice to know that the toolchain can build itself so you know you system isn't going to be catastrophically unstable.

I know that can be a little tricky to wrap your head around, but luckily, these two stages rarely get changed so you don't HAVE to completely understand it (although you definitely should).

#### Stage 3

Stage 3 is the basic GNU/Linux system.  You have the basic [Linux Filesystem Hierarchy](https://www.man7.org/linux/man-pages/man7/hier.7.html) and `gnutls` like sed, awk, bash.  Stage 3 is completely usable and you could stop here.  In fact, when you are installing Gentoo manually, you [start with a stage 3](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Stage).  But stage 3 is boooooring. If you want to customize your OS, you will need to that in stage 4

#### Stage 4

Stage 4 is basically just stage 3 + a kernel + whatever tools you want.  If you want to install Gnome, ZFS, and Grub, you would do this here.  There is a little bit of a song and dance to get certain kernel-related packages installed (I'm looking at you ZFS), but that can easily be worked around be creating a second stage4 based on the previous stage 4 (stage4-amd64-default-zfs depends on stage4-amd64-default for example).

Since you can base stage 4s on other stage 4s, there's no stage 5+.  Just more stage 4.

#### Live CDs.

You can build live cds based on your stage 3s, but I haven't quite figured this part out so I'll resserve my explanation for a later post.

## Building

Ok, we have a general understanding of what the different stages are, how do we use them?  As always, the magic is in the `Makefile`.  Simply run `make targets` in your terminal and you'll be presented with the stages and individual targets for building.  You can install just the individual stage by typing `sudo make stage #`, or build them all by running `sudo make`.  You will then be prompted for quay.io and or docker hub credentials.  The build script will only push to the repo(s) youprovide credentials for.  You can also pass the credentials via environment args with something like `QUSER=foo QPASS=bar DUSER=baz sudo make`.  That command will take the `QUSER` and `QPASS` credentials for quay.io and will prompt you for the password for the DUSER that you provided for docker hub.

If you want to customize the stage for, take a look at the stage for containerfiles and make targets to build your own.

## Conclustion

To wrap it up, OSB automates the worst parts of configuring Gentoo while still giving you all the benefits of its flexibility.  It allows you to have a fully customized and infinitely up to date system always at your fingertips.
