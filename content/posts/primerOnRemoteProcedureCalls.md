+++
title = "Primer On Remote Procedure Calls"
date = "2023-01-16T07:56:34-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "omscs", "cs7210"]
keywords = ["", ""]
description = "Notes about Lesson 2 of CS-7210"
showFullContent = false
readingTime = false
hideComments = false
#draft = true
+++

# Primer on Remote Procedure Calls

These are my notes on the primer for RPC lesson

## Summary

- [Brief overview of RPC](./#client-server-architecture)
- [Role of RPC in client-server systems](./#role-of-rpc)
- Requirements of RPC
- [Examples](./#grpc)

## Client-Server Architecture

Simple pattern for distributed system

1. Client sends request to server
1. Server sends response to client

### Challenges with Client-Server

1. Discovery and binding
1. Identifying the interface and parameter types
1. Agreeing on data representation
    - Null terminated strings?
    - Array ordering?
1. Eplicit data management
1. Unpredictable delays
1. Unknown cause of failures

## Role of RPC

- Aims to hide complexity of distrubted programming from application and
developers.
- Make distributed programming appear similar to local node programming.

### Functionality

- Service registration mechanism
- Connection management
- Interface specification
- Type system
- Data management
  - (De)Serialization/(Un)Marshaling
- Error management

## Architecture of RPC System

- API
  - Calling a remote procedure
- Stubs
  - Data marshaling
- Runtime
  - Connection management, failures

> Interface Definition Language (IDL) := The agreed-upon way in which
> servers describe the services the provide and their associated data
> requirements.

### Creation Sequence

1. Programmer writes IDL
1. Compiler compiles IDL into stubs
1. Server code implements stubs
1. Server added to registry
1. Programmer writes client against API
1. When code is run, rumtime handles everything else

## Anatomy of RPC Call

1. Client calls procedure
1. (Client) Stub builds message
1. Message is sent (potentially over the network)
1. Server OS hands message
1. (Server) Stub unpacks message
1. Stub makes local call to procedure

## Invocation Semantics

Synchronous vs Async

> Synchronous

Thread blocks when making a call

> Asynchronous

Threads do not block when they make a call

Hides latency

Can register a callback function that gets called when data is returned.

On a local PC, no response => deadlock or full failure
    - Has to restart and redo everything

Using Remote PC, No response doesn't tell us _why_ it failed

`At Most Once` => if client knows calls may not be called, timeout/retransmit
logic can be moved to client

## Examples

- Sun RPC
- SOAP
- CORBA
- Apache Thrift
- gRPC
- Other specialized implementations
    - useful for when you can make certain gaurantees about the environment
        - high speed, reliable network
        - low resource/embedded footprint

## gRPC

[gRPC main page](https://grpc.io/)

[gRPC c++ quickstart](https://grpc.io/docs/languages/cpp/quickstart/)

Googles RPC implementations

Inspired by SunRPC

Relies on `Protocol Buffers`

> Protocol Buffers := An IDL that allows programmers to define types and
> operations used in an RPC system.  RPC mechanisms then use the protocol
> buffers to perform data serialization.

`protoc` compiler translates `*.proto` -> stubs

```protobuf
// Copyright 2015 gRPC authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Example proto file from https://github.com/grpc/grpc/blob/master/examples/protos/helloworld.proto

syntax = "proto3";

option java_multiple_files = true;
option java_package = "io.grpc.examples.helloworld";
option java_outer_classname = "HelloWorldProto";
option objc_class_prefix = "HLW";

package helloworld;

// The greeting service definition.
service Greeter {
  // Sends a greeting
  rpc SayHello (HelloRequest) returns (HelloReply) {}
  // Sends another greeting
  rpc SayHelloAgain(HelloRequest) returns (HelloReply) {}
}

// The request message containing the user's name.
message HelloRequest {
  string name = 1;
}

// The response message containing the greetings
message HelloReply {
  string message = 1;
}

```

`service` blocks define methods/interfaces

`message` blocks define data types

do _**NOT**_ change index of message field after publishing

