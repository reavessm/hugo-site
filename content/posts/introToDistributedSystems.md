+++
title = "Intro To Distributed Systems"
date = "2023-01-13T05:20:50-05:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
cover = ""
tags = ["programming", "omscs", "cs7210"]
keywords = ["", ""]
description = "Notes about Lesson 1 of CS-7210"
showFullContent = false
readingTime = "1:18"
hideComments = false
+++

# Introduction to Distributed Systems

These are my notes summarizing Lesson 1 of CS-7210.

## Summary

- [Characteristics](./#characteristics-of-a-distributed-system)
- [Role of Models](./#role-of-models)
- [CAP Theorem](./#cap-theorem)

## Characteristics of a Distributed System

> Distributed System := A collection of computing resources, connected via
> some network that behaves as a single system.

There is no (theoretical) limit
to the number of computing resources, typically called `nodes`, but the number
of nodes, and other complexities, should be invisible to the end user.

A distributed system should also be able to scale, meaning add nodes to or
remove nodes from the system, as well as have some degree of [fault
tolerance](https://en.wikipedia.org/wiki/Fault_tolerance).

## Role of Models

We typically represent distributed sytems as a
[graph](https://en.wikipedia.org/wiki/Graph_drawing).  We represent nodes as
vertices and communication between nodes as edges.  In more complex models, you
can even represent things like
[state](https://en.wikipedia.org/wiki/State_(computer_science)) by labeling the
nodes and changes in state then change the label (going from `S1` to `S1'`, for
example).

```dot
digraph Example {
  graph [fontname = "FiraCode"];
  node [fontname = "FiraCode"];
  edge [fontname = "FiraCode"];

  bgcolor=transparent;

  s1 [label="S1"];
  s1_p [label="S1`"];

  s1 -> s1_p;
}
```

The main considerations for models are that they `accurately represent the
problem` and `allow the solution to be analyzed`

## CAP Theorem

The [CAP Theorem](https://en.wikipedia.org/wiki/CAP_theorem) is pretty
essential to distributed sytems.  It basically describes the idea that systems
can be at most two of consistent, available, or partition tolerant.

> Consistent := Every node in the system has accurate access to the most recent
> information.

> Available := Every request gets a valid response.

> Partition Tolerant := The system continues to operate regardless of
> communication failures.

There are also extensions to the CAP Theorem, such as the [PACELC
Theorem](https://en.wikipedia.org/wiki/PACELC_theorem) which states that you
only need to choose between the original three characteristics when there is a
partition, otherwise you additionally need to choose between latency and
consistency.
