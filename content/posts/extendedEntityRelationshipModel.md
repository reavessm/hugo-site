+++
title = "Extended Entity Relationship Model"
date = "2023-05-29T10:28:24-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 3 of CS-6400"
showFullContent = false
readingTime = false
hideComments = false
+++

## Summary

- [Entity Types and Entity Surrogates](#entity-types-and-entity-surrogates)
- [Single Valued Properties](#single-valued-properties)
- [Identifying Properties](#identifying-properties)
- [Composite Properties](#composite-properties)
- [Multi Valued Property Types](#multi-valued-property-types)
- [Relationship Types](#relationship-types)
- [Recursive relationship type](#recursive-relationship-type)
- [Supertypes and Subtypes](#supertypes-and-subtypes)
- [Union Entity Types](#union-entity-types)
- [Relationship Objectification](#relationship-objectification)
- [What can EER do?](#what-can-eer-do?)
- [What about Queries?](#what-about-queries?)
- [Relational Model](#relational-model)
  - [Data Structures](#data-structures)
  - [Tables](#tables)
  - [Constraints](#constraints)

## Entity Types and Entity Surrogates

Entity type names must be unique.

Entities are represented by rectangles.

Properties are represented by ovals.

```graphviz
digraph G {
    User [shape=rect; style=rounded];
}
```

## Single Valued Properties

Single Valued Properties only have the property name in the oval.

Proptery values are:
  - Lexical
  - Visible
  - Audible
  - Things that name other things

```graphviz
graph G {
    User [shape=rect; style=rounded];
    Email;
    Password;
    
    User -- Email;
    User -- Password;
}
```

## Identifying Properties

An underlined property type identifies the entity

```graphviz
graph G {
    User [shape=rect; style=rounded];
    Email [label=<<u>Email</u>>];
    Password;
    
    User -- Email;
    User -- Password;
}
```

This is the primary key.

Every entity must be uniquely referenceable.

## Composite Properties

The value of some properties may be composed of other properties.

```graphviz
graph G {
    User [shape=rect; style=rounded];
    Email [label=<<u>Email</u>>];
    Password;
    
    User -- Email;
    User -- Password;
    
    User -- Name;
    Name -- FirstName;
    Name -- LastName;
}
```

## Multi Valued Property Types

Modeled by double oval.

An entity can have multiple multi-valued properties.

For example, a user can have multiple interests.

```graphviz
graph G {
    Interest [shape=oval; peripheries=2];
    User [shape=rect; style=rounded];
    
    User -- Interest;
}
```

## Relationship Types

Represented by diamonds, cardinality is represented by numbers on the edges.

A `partial function` is a mapping between entities that may not include every
instance of a given entity.

```graphviz
graph G {
    rank=same;
    rankdir=LR;
    
    mu [label="Male User"; shape=rect; style=rounded];
    fu [label="Female User"; shape=rect; style=rounded];
    cm [label="Current\nMarriage"; shape=diamond];
    
    mu -- cm [label="1"];
    cm -- fu [label="1"];
}
```

Having `N` instead of a number means "many"

Mandatory relationships have a bold line and are called a `total function`.

```graphviz
graph G {
    rank=same;
    rankdir=LR;
    
    node [shape=rect; style=rounded];
    
    e [label="Employer"];
    u [label="User"];
    j [label="Current\nJob"; shape=diamond; style=""];
    
    e -- j [label="1"];
    j -- u [label="N"; style="bold"];
}
```

You can also have N-M relationship types.

```graphviz
graph G {
    rank=same;
    rankdir=LR;
    
    node [shape=rect; style=rounded];
    
    s [label="School"];
    u [label="User"];
    a [label="Attended", style="", shape=diamond];
    
    s -- a [label="N"];
    a -- u [label="M"];
}
```

Relationships can be between more than two entities.

```graphviz
graph G {
    beautify=true;

   subgraph relationships {
        node [shape=diamond];

        etm [label="Event\nTeam\nMember"];
    }
    
    subgraph entities {
        node [shape=rect; style=rounded];
    
        u [label="User"];
        t [label="Team"];
        e [label="Event"];
    }    
    
    subgraph properties {
        email [label=<<u>Email</u>>];
        en [label=<<u>Event Name</u>>];
        tn [label=<<u>Team Name</u>>];
        p [label="Position"];
    }

    etm -- u [label="L"];
    etm -- e [label="N"];
    etm -- t [label="M"];
    
    u -- email;
    e -- en;
    t -- tn;
    etm -- p;
}
```

Many ternary relationship types cannot be reduced to a conjunction of binary
relationship types.

Imagine how the below is different from the above.

```graphviz
graph G {
    beautify=true;

   subgraph relationships {
       rank=max;
        node [shape=diamond];

        ut [label="User\nTeam"];
        ue [label="User\nEvent"];
        te [label="Team\nEvent"];
    }
    
    subgraph entities {
        node [shape=rect; style=rounded];
    
        u [label="User"];
        e [label="Event"];
        t [label="Team"];
    }    
    
    subgraph properties {
        rank=min;
        email [label=<<u>Email</u>>];
        en [label=<<u>Event Name</u>>];
        tn [label=<<u>Team Name</u>>];
    }

    u -- ut -- t;
    u -- ue -- e;
    e -- te -- t;
    
    u -- email;
    e -- en;
    t -- tn;
}
```

Entities can be identified by their relationships, or a combination of their
relationshiops and their properties.

```graphviz
graph G {
    beautify=true;

    subgraph relationships {
        rank=max;
        node [shape=diamond];

        p [label="Posted"; peripheries=2];
    }
    
    subgraph entities {
        node [shape=rect; style=rounded];
    
        u [label="User"];
        s [label="Status"; peripheries=2];
    }    
    
    subgraph properties {
        rank=min;
        email [label=<<u>Email</u>>];
        dt [label=<<u>DateAndTime</u>>; style=dashed];
    }
    
    s -- p -- u;
    u -- email;
    s -- dt;
}
```

In the above, `Status` cannot:
  - Exist without `User`;
  - Be identified without `User`;

`DateAndTime` and `Email` identify `Status`.

`Status` is therefore a weak entity type and `DateAndTime` would be a `partial
identifier`.

Note, `DateAndTime` should be a regular oval with a dashed underline, but idk
how to do that in graphviz.

## Recursive relationship type

Recursive relationship types relate an entity back to itself.

```graphviz
digraph G {
    beautify=true;
    edge [arrowhead=none];

    subgraph relationships {
        rank=max;
        node [shape=diamond];

        m [label="Manages"];
    }
    
    subgraph entities {
        node [shape=rect; style=rounded];
    
        u [label="AdminUser"];

    }    
    
    subgraph properties {
        rank=min;
        email [label=<<u>Email</u>>];
        sd [label="SinceDate"];
    }

    m -> u [
            arrowhead=normal;
            headlabel="Supervisee";
            labeldistance=4; 
            labelangle=75;
        ];
    u -> m [
            arrowhead=normal;
            headlabel="Supervisor";
            labeldistance=4; 
            labelangle=75;
        ];
    
    u -> email;
    u -> sd;
}
```

## Supertypes and Subtypes

A subtype "is-a" supertype.

For instance, every male user is-a user.

The "is-a" relationship is modeled with circle and either a "d" if it is
disjoint or a "o" if it allows for overlap.

Properties are inherited similar to OOP.

```graphviz
digraph G {
  beautify=true;
  edge [arrowhead=none];

  subgraph entities {
    node [shape=rect; style=rounded];

    u [label="User"];
    f [label="Female"];
    m [label="Male"];
    r [label="RegularUser"];
    a [label="AdminUser"];
  }

  subgraph inheritance {
    node [shape=circle];
    edge [arrowhead=normal];

    d;
    o;

    m -> d;
    f -> d;
    d -> u;

    r -> o;
    a -> o;
    o -> u;
  }

  subgraph properties {
    email [label=<<u>Email</u>>];
    p [label="Passoword"];
    ll [label="Last Login"];
    bd [label="Birth Date"];
    cc [label="Current City"];

    bd -> r;
    cc -> r;
    ll -> a;

    email -> u;
    p -> u;
  }
}
```

## Union Entity Types

Union entity types are composed of multiple different entities where an
instance of the union has to be a instance of 1 and only 1 subset entity.

```graphviz
digraph G {
  beautify=true;
  edge [arrowhead=none];

  subgraph entities {
    node [shape=rect; style=rounded];

    u [label="User"];
    e [label="Employer"];
    c [label="Company"];
    g [label="Govt Agency"];
  }

  subgraph relationships {
    node [shape=diamond];
    
    cj [label="Current\nJob"];
    
    u -> cj -> e;
  }

  subgraph inheritance {
    node [shape=circle];
    edge [arrowhead=normal];

    emp [label="U"];

    e -> emp [style="bold"; arrowhead=none];
    emp -> c;
    emp -> g;
  }

  subgraph properties {
    ein [label="EIN#"];
    aid [label="Agency Id"];
    an [label="Agency Name"];
    m [label="Municipality"];
    
    ein -> c;
    
    an -> aid;
    m -> aid;
    aid -> g;
  }
}
```

## Relationship Objectification

In order to turn a relationship into an entity, you split the relationship into
two and add an entity between them.

So this:

```graphviz
digraph G {
  beautify=true;
  edge [arrowhead=none];

  subgraph entities {
    node [shape=rect; style=rounded];

    u [label="User"];
    s [label="School"]
  }

  subgraph relationships {
    node [shape=diamond];
    
    sa [label="Schools\nAttended"];
    
    u -> sa [label= "N"]
    sa -> s [label="M"];
  }

  subgraph inheritance {
    node [shape=circle];
    edge [arrowhead=normal];
  }

  subgraph properties {
    rank=same;
    rankdir=LR;
    gpa [label="GPA"];
    
    gpa -> sa;
  }
}
```

Becomes this:

```graphviz
digraph G {
  beautify=true;
  edge [arrowhead=none];

  subgraph entities {
    node [shape=rect; style=rounded];

    u [label="User"];
    s [label="School"];
    sa [label="Schools Attended"];
  }

  subgraph relationships {
    node [shape=diamond];
    
    sa1 [label=""];
    sa2 [label=""];
    
    u -> sa1 [label= "1"];
    sa1 -> sa [label="N"];
    sa2 -> s [label="1"];
    sa -> sa2 [label="M"];
  }

  subgraph inheritance {
    node [shape=circle];
    edge [arrowhead=normal];
  }

  subgraph properties {
    rank=same;
    rankdir=LR;
    gpa [label="GPA"];
    
    gpa -> sa;
  }
}
```

## What can EER do?

Classification is handled by entity types.

Generalization is handled by Super/Sub types.

Aggregation is not explicitly handled by EER.

Great for fixing and representing a perception of reality.

## What about Queries?

Queries do not have a type in EER.

The closest thing is a list of properties.

This is why DBMS's don't rely on EER and instead use relational data models.

## Relational Model

- Data Structures
- Constraints
- Operations
  - Algebra
  - Calculus
    - Tuple Calculus (SQL)
    - Domain Calculus (QBE)

### Data Structures

The only structure is relations.

> Domain := a set of atomic values

> Relation := a subset of the set of ordered n-tuples in a domain.

$katex
R \subset {<d_1, d_2, ..., d_n> | d_i \existsIn D_i, i=1, ..., n}
katex$

> Attribute := a unique name given to a domain in a relation helping us
> interpret domain values.

### Tables

We illustrate relations by tables.

RegularUser:

| Email varchar(50) | BirthDate datetime | CurrentCity varchar(50) | Hometown varchar(50) | Salary integer |
| :---              | :---:              | :---:                   | :---:                | ---:           |
| user1@gt.edu      | 1985-11-07         | Seattle                 | Atlanta              | 10,000         |

- Relation name
  - RegularUser
- Attribute names
  - Email
  - BirthDate
  - CurrentCity
  - Hometown
  - Salary
- Domains
  - varchar(50)
  - datetime
  - integer
- Tuples
  - Rows
- Degree
  - Number of columns
- Cardinality
  - Number of rows

Order of attributes and tuples are idenpendent of relation

### Constraints

- Keys
  - Entity Integrity
- Primary Keys
  - Referntial Integrity

If an attribute is a key of a table, it can't be null or duplicated.

If an attribute references a key of another table, it must be a subset of the
values of that key.
