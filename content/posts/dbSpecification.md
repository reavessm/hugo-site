+++
title = "DB Specification"
date = "2023-06-05T05:09:32-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs6400"]
description = "Notes about Lesson 4 of CS-6400"
+++

## Summary

- [Constraints](#constraints)
- [Task Decomposition](#task-decomposition)

## Constraints

There are some constraints that can't be modeled in EER and need to be handled
in app code.

## Task Decomposition

There are times where you need to break up a task to better fit how a db models
it.

- Lookups vs Insert/Delete/Updates
  - Grouping reads together and writes together can help minimize what types of
    locks are needed for a given action.
- Number of schema constructs
  - Minimizing this number lowers the number of locks needed.
- Enabling conditions
  - Keeping enabling conditions consistent ensures the task can be scheduled.
- Keep task frequencies consistent
  - This can impact the usefulness of indexes.
- Is consistency essential?
  - Atomic transactions are not always needed.
- Is a mother task needed to control subtasks?
