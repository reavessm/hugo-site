+++
title = "Edge Computing And IoT"
date = "2023-04-08T08:08:05-04:00"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["programming", "omscs", "cs7210"]
description = "Notes about Lesson 17 of CS-7210"
showFullContent = false
readingTime = false
hideComments = false
+++

## Required Readings

- [The Computing Landscape of the 21st Century](http://elijah.cs.cmu.edu/DOCS/satya-hotmobile2019.pdf)

### Optional

- [Transactuations](https://www.usenix.org/system/files/atc19-sengupta.pdf)
- [The Emerging Landscape of Edge Computing](https://www.microsoft.com/en-us/research/uploads/prod/2020/02/GetMobile__Edge_BW.pdf)

## Summary

- [Tiers in Computing](#tiers-in-computing)
- [Why Edge Computing](#why-edge-computing)
- [Closing the Latency-Bandwidth Gap](#closing-the-latency-bandwidth-gap)
- [Is Edge Computing New](#is-edge-computing-new)
- [Edge Computing Drivers](#edge-computing-drivers)
- [Distributed Edge Computing](#distributed-edge-computing)
- [IoT and Distributed Transactions](#iot-and-distributed-transactions)
- [Transactuations](#transactuations)
  - [Transactuation Invariants](#transactuation-invariants)
- [Evaluation of Transactuations](#evaluation-of-transactuations)
  - [Programmability](#programmability)


## Tiers in Computing

Demand for lower latency drives compute towards where data is collected

Clouds -> Cloudlets (luggable, vehicular, mini-datacenter) -> IOT (drones,
security cameras, smartphones) -> RFID tags

## Why Edge Computing

Newer workloads (HD Video, AR/VR, SmartCity & Automation) increase demand for
bandwidth-intensive and latency-sensitive systems.

Working from home also shifts connectivity needs.

## Closing the Latency-Bandwidth Gap

5G helps, but might be too little, too late.

Transitioning to newer software stacks has been traditionally slow, but is
getting faster due to trends of Open Source Software and commodity hardware.

Moving computation to the closer to devices at the users is called edge
computing.

## Is Edge Computing New

CDNs have existed for a long time.  CDNs are basically just caches, and still
don't hit the scale of edge computing.

## Edge Computing Drivers

- Speed of Light
  - Data can only move so fast
- Data growth
- New Apps
  - 5G
  - Video
  - AR/VR
  - IoT
  - Cognitive tasks
  - Enterprise/private LTE
- Data Sovereignty laws

## Distributed Edge Computing

Edge != Cloud

Scale and geo-distribution are different

"Chatty" protocols are not appropriate
  - heartbeats, etc

Edge is not elastic

Mobility, device churn, reliability

Variability/heterogeneity

Localization, contextualization

Decentralization

## IoT and Distributed Transactions

Smart devices -> edge gateways -> Cloud
  - Sensors and Actuation

Physical state != application state

## Transactuations

High level programming abstraction and model

```text
perform(applicationLogic,
        [sensorList, timeWindow, sensingPolicyt],
        [actuatingPolicy])
```

`onSuccess()` and `onFailure()` methods

Provide atomic durability of actuations

### Transactuation Invariants

- Sensing invariant
  - Transactuation executes only when staleness of its sensor reads is bounded,
    as per specified sensing policy.
- Sensing policy
  - How much staleness is acceptable?
  - How many failed sensors are acceptable?
    - At least one CO2 sensor must be read within last 5 minutes
- Actuation invariant
  - When a transactuation commits its app states, enough actuations have
    succeeded as per actuation policy.
- Actuation policy
  - How many failed actuations are acceptable?
    - At least one alarm should turn on.

## Evaluation of Transactuations

Evaluation Questions:
  - Impact on programmability
    - Typically lines of code
  - Impact on performance during failure-free execution
    - Benchmark before and after adding safety measures
  - Impact on correctness during failures
    - Unit/e2e tests before and after adding safety measures


### Programmability

| Application        | Original App | Original App + Consistency | Transactuations |
|:--                 |:--:          |:--:                        |:--:             |
|Rise and Shine      | 72           | 195                        | 68              |
|Whole house fan     | 29           | 176                        | 26              |
|Thermostat auto off | 70           | 198                        | 68              |
