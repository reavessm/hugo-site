---
title: "Showcase"
---

This is a collection of projects that I am proud of.

> **[REA](https://gitlab.com/reavessm/rea)**

`rea` is a **R**everse-**E**ngineering **A**pi generator

{{< code language="markdown" id="1" expand="Show" collapse="Hide" isCollapsed="true" >}}

## Usage

```
> rea -h
Usage of rea:
  -f string
        File to read from
  -o string
        File to output to (default "out.go")
  -p string
        Package name of generated file (default "main")
  -s string
        Name of struct to be generated (default "Object")
  -v    Verbose output?
```

## Examples

### Basic

```sh
curl 'https://www.boredapi.com/api/activity' | rea && cat out.go
```

### Advanced

```sh
rea -w 'https://randomuser.me/api/' -s User -p accounts -o user.go && cat user.go
```

### Appending

```sh
rea -w https://www.boredapi.com/api/activity -o foo.go -s activity
ocm whoami | rea -o foo.go -s user
cat foo.go
```

## Installation

```sh
go install gitlab.com/reavessm/rea/cmd/rea@latest
```

{{< /code >}}

> **[OSB](https://gitlab.com/reavessm/osb)**

`osb` is a personal **O**perating **S**ystem **B**uilder based on Gentoo

{{< code language="markdown" id="2" expand="Show" collapse="Hide" isCollapsed="true" >}}

## Usage

Building and pushing is done with `make`.  If you want to rebuild everything,
simply run `sudo make`.  If you only want to build a specific stage (like
stage4), run `sudo make stage4`.  If you want a more specific target, run
`sudo make stage4-amd64-default-zfs`.  If you want to see all available
containers, run `make targets`.

## Targets

Installable targets are:

```
all
├── stage0
│   ├── portage
│   └── catalyst
├── stage1
│   └── stage1-amd64-default
├── stage2
│   └── stage2-amd64-default
├── stage3
│   └── stage3-amd64-default
└── stage4
    ├── stage4-amd64-default
    └── stage4-amd64-default-zfs
```

The default target is 'all'

{{< /code >}}
