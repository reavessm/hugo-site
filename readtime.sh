#!/bin/bash

count=$(sed -n '/^\#/,$p' $1 | wc -w)

min=$(echo "${count} / 200" | bc)
sec=$(echo "((${count} / 200) - 1) * 60" | bc -l)

echo "${min}:${sec}" | sed 's/\..*$//'
